//
//  User+CoreDataProperties.swift
//
//
//  Created by Kirill Dobryakov on 8/26/18.
//
//

import Foundation
import CoreData


extension User {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }
    
    @NSManaged public var birthdate: NSDate?
    @NSManaged public var gender: Gender
    @NSManaged public var id: Int64
    @NSManaged public var isInstagramConnected: Bool
    @NSManaged public var name: String?
    @NSManaged public var profession: String?
    @NSManaged public var workPlace: String?
    @NSManaged public var isNewlyRegistered: Bool
    @NSManaged public var studyPlace: String?
    @NSManaged public var about: String?
    @NSManaged public var giftPhone: String?
    @NSManaged public var talents: [String]
    @NSManaged public var settings: Settings?
    @NSManaged public var totems: NSOrderedSet
    @NSManaged public var instaPhotos: NSOrderedSet
    @NSManaged public var photos: NSOrderedSet
    
}

// MARK: Generated accessors for instaPhotos
extension User {
    
    @objc(insertObject:inInstaPhotosAtIndex:)
    @NSManaged public func insertIntoInstaPhotos(_ value: InstaPhoto, at idx: Int)
    
    @objc(removeObjectFromInstaPhotosAtIndex:)
    @NSManaged public func removeFromInstaPhotos(at idx: Int)
    
    @objc(insertInstaPhotos:atIndexes:)
    @NSManaged public func insertIntoInstaPhotos(_ values: [InstaPhoto], at indexes: NSIndexSet)
    
    @objc(removeInstaPhotosAtIndexes:)
    @NSManaged public func removeFromInstaPhotos(at indexes: NSIndexSet)
    
    @objc(replaceObjectInInstaPhotosAtIndex:withObject:)
    @NSManaged public func replaceInstaPhotos(at idx: Int, with value: InstaPhoto)
    
    @objc(replaceInstaPhotosAtIndexes:withInstaPhotos:)
    @NSManaged public func replaceInstaPhotos(at indexes: NSIndexSet, with values: [InstaPhoto])
    
    @objc(addInstaPhotosObject:)
    @NSManaged public func addToInstaPhotos(_ value: InstaPhoto)
    
    @objc(removeInstaPhotosObject:)
    @NSManaged public func removeFromInstaPhotos(_ value: InstaPhoto)
    
    @objc(addInstaPhotos:)
    @NSManaged public func addToInstaPhotos(_ values: NSOrderedSet)
    
    @objc(removeInstaPhotos:)
    @NSManaged public func removeFromInstaPhotos(_ values: NSOrderedSet)
    
}

// MARK: Generated accessors for photos
extension User {
    
    @objc(insertObject:inPhotosAtIndex:)
    @NSManaged public func insertIntoPhotos(_ value: Photo, at idx: Int)
    
    @objc(removeObjectFromPhotosAtIndex:)
    @NSManaged public func removeFromPhotos(at idx: Int)
    
    @objc(insertPhotos:atIndexes:)
    @NSManaged public func insertIntoPhotos(_ values: [Photo], at indexes: NSIndexSet)
    
    @objc(removePhotosAtIndexes:)
    @NSManaged public func removeFromPhotos(at indexes: NSIndexSet)
    
    @objc(replaceObjectInPhotosAtIndex:withObject:)
    @NSManaged public func replacePhotos(at idx: Int, with value: Photo)
    
    @objc(replacePhotosAtIndexes:withPhotos:)
    @NSManaged public func replacePhotos(at indexes: NSIndexSet, with values: [Photo])
    
    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: Photo)
    
    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: Photo)
    
    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSOrderedSet)
    
    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSOrderedSet)
    
}

// MARK: Generated accessors for totems
extension User {
    
    @objc(insertObject:inTotemsAtIndex:)
    @NSManaged public func insertIntoTotems(_ value: Totem, at idx: Int)
    
    @objc(removeObjectFromTotemsAtIndex:)
    @NSManaged public func removeFromTotems(at idx: Int)
    
    @objc(insertTotems:atIndexes:)
    @NSManaged public func insertIntoTotems(_ values: [Totem], at indexes: NSIndexSet)
    
    @objc(removeTotemsAtIndexes:)
    @NSManaged public func removeFromTotems(at indexes: NSIndexSet)
    
    @objc(replaceObjectInTotemsAtIndex:withObject:)
    @NSManaged public func replaceTotems(at idx: Int, with value: Totem)
    
    @objc(replaceTotemsAtIndexes:withTotems:)
    @NSManaged public func replaceTotems(at indexes: NSIndexSet, with values: [Totem])
    
    @objc(addTotemsObject:)
    @NSManaged public func addToTotems(_ value: Totem)
    
    @objc(removeTotemsObject:)
    @NSManaged public func removeFromTotems(_ value: Totem)
    
    @objc(addTotems:)
    @NSManaged public func addToTotems(_ values: NSOrderedSet)
    
    @objc(removeTotems:)
    @NSManaged public func removeFromTotems(_ values: NSOrderedSet)
    
}
