//
//  User.swift
//  Totemo
//
//  Created by Kirill Dobryakov on 12/9/17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Interest: String{
    case male
    case female
    case all
    
    static let interests = [male, female, all]
}

class User: NSObject {

    static var me: User?
    
    var talents = ["Рисование", "Гитара", "Кулинария", "Публичные выступления"]
    
    let id: Int
    var name: String?
    var gender: Gender?
  
    var birthday: Date?
    var isNewlyRegistered: Bool?

    var profession: String?
    var workPlace: String?
    var studyPlace: String?
    var about: String?
    
    var giftPhone: String?
    
    var instagramConnected: Bool?
    
    var photos: [Photo]?
    var instaPhotos: [InstaPhoto]?
    
    var totems: [Totem]?
    
    var isMissingCriticalInfo: Bool{
        return name == nil || gender == nil || birthday == nil
    }
    


    
    override init(){
        self.id = -1
        super.init()
    }
    
    convenience init?(jsonForRegistration: JSON) {
        self.init(json: jsonForRegistration["user"])
        self.isNewlyRegistered  = jsonForRegistration["is_newly_registered"].bool
    }
    
    init?(json: JSON) {
        
      

        guard let id = json["id"].int else {pr("no user id"); return nil}

        if let genderIndex = Int(json["gender"].stringValue)  {
            self.gender = Gender.genders[genderIndex]
        }
        
        self.name   = json["name"].string
        self.id     = id
        self.birthday = json["birthday"].stringValue.toDate(format: Dates.serverBirthdayFormat)
        self.about = json["profile"]["about"].string
        self.profession = json["profile"]["profession"].string
        self.workPlace = json["profile"]["work"].string
        self.studyPlace = json["profile"]["education"].string
        self.instagramConnected = json["is_instagram_attached"].boolValue
        
    }
}

