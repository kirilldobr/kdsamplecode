//
//  FooterButtonCollectionView.swift
//  Totemo
//
//  Created by Kirill Dobryakov on 4/10/18.
//  Copyright © 2018 Kirill Dobryakov. All rights reserved.
//

import UIKit

class FooterButtonCollectionView: UICollectionReusableView {

    weak var delegate: FooterButtonCellDelegate?
    
    @IBOutlet weak var button: TotemoButton!
    @IBAction func buttonPressed(_ sender: Any) {
        delegate?.footerButtonPressed()
    }
}
 
