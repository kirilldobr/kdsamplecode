//
//  ProfilePhotoTableViewController+UICollectionView.swift
//  Totemo
//
//  Created by Kirill Dobryakov on 1/30/18.
//  Copyright © 2018 Kirill Dobryakov. All rights reserved.
//

import UIKit
import DKImagePickerController


extension ProfilePhotoCollectionViewController: PhotoCellDelegate{
    
    func openPhoto(sender: UITapGestureRecognizer) {
        let indexPath = collectionView!.indexPathForItem(at: sender.location(in: collectionView!))!
        let referenceCell = collectionView!.cellForItem(at: indexPath)!
        OpenPhotoHelper.open(photo: photos[indexPath.item], in: self, referenceView: referenceCell)
    }
    
    func photoCellAddPhotoPressed() {
        let selectedAssets = photos.filter{$0.type == .local}.map{$0.asset!}
        let context = CoreDataStack.shared.managedContext
        ImagePickerHelper.showImagePicker(in: self, selectedAssets: selectedAssets) { (assets) in
            CoreDataStack.shared.managedContext.perform {
                for assetItem in assets{
                    if !selectedAssets.contains(assetItem) {
                        self.photos.append(Photo(dkAsset: assetItem, entity: Photo.entity(), insertInto: context))
                        self.collectionView!.reloadData()
                    }
                }
                self.collectionView!.reloadData()
            }
        }
    }
    
    private func removePhotoFromCollectionView(index: Int){
        
        if photos[index].type == .local{
            photos.remove(at: index)
        } else {
            User.me!.removeFromPhotos(photos[index])
            CoreDataStack.shared.managedContext.delete(photos[index])
            photos.remove(at: index)
        }
        collectionView!.performBatchUpdates({
            self.collectionView!.deleteItems(at: [IndexPath(item: index, section: 0)])
        }, completion: {x in
            self.setAvatarIndicator()
        })
    }
    
    func photoCelldeletePhotoPressed(sender: UIButton) {
    
        if photos.count == 1 {
            showAlert(title: "Внимание!", message: "Должна остаться хотя бы одна фотография!")
            return
        }
        
        let item = collectionView!.indexPath(for: sender.superview!.superview! as! UICollectionViewCell)!.item
        
        if photos[item].type == .local{
            removePhotoFromCollectionView(index: item)
        }
        else{
            APIUserPhoto.shared.deletePhoto(photo: photos[item], success: { [weak self] in
             
                guard let `self` = self else {
                    return
                }
                
                self.removePhotoFromCollectionView(index: item)
            }, failure: { (error) in pr(error) })
        }
    }
    
}



