//
//  User+CoreDataClass.swift
//  
//
//  Created by Kirill Dobryakov on 8/24/18.
//
//

import Foundation
import CoreData
import SwiftyJSON

@objc(User)
public class User: MyManagedObject{
    lazy var context = CoreDataStack.shared.managedContext
    internal static var myUser: User?
    
    
    static var me: User? = {
        guard let userIDURL = UserDefaults.standard.object(forKey: "myID") as? String else{
            pr("no user, something is really wrong")
            return nil
        }
        
        if let myUser = myUser{
            return myUser
        }
        
        do {
            myUser = try CoreDataStack.shared.managedContext.existingObject(with: CoreDataStack.shared.managedIDFor(url: URL(string: userIDURL)!)!) as? User
            return myUser
        } catch {
            return nil
        }
    }()
    
    var age: Int?{
        get {
            if let birthdate = birthdate{
                return Calendar.current.dateComponents([.year], from: birthdate as Date, to: Date()).year
            }
            return nil
        }
    }
    
    var isMissingCriticalInfo: Bool{
        return name == nil || birthdate == nil
    }
    
    
    var filteredTotemsArray: [Totem]{
        get{
            return (totems.array as! [Totem]).filter{$0.places.count > 0}
        }
    }
    
    
    func getPhotos() {
        APIUserPhoto.shared.getPhotos(for: id, success: { [weak self] (photoJSONs) in
            guard let self = self else {
                return
            }
            
            self.context.perform {
                
    
                photoJSONs.forEach {
                    self.changeOrAddPhoto(with: $0)
                }
                //TODO: delete leftover photos
                let photoIDsFromServer = photoJSONs.map{$0["id"].intValue}
                _ = self.photos .filter {!photoIDsFromServer.contains(Int(($0 as! Photo).id))} .map{
                        self.removeFromPhotos(($0 as! Photo))
                        self.context.delete($0 as! NSManagedObject)
                }
//
                self.context.performSave()
            }
        }) { (error) in pr("couldnt load profile photos") }
    }
    
    func getInstaPhotos() {
        APIInstagram.shared.getPhotos(for: id, success: { [weak self] (photoJSONs) in
           
            
            guard let self = self else {
                return
            }
            
            self.context.perform {
               // let photoIDs = User.me!.instaPhotos.map {($0 as! InstaPhoto).id}
                photoJSONs.forEach {
                    self.changeOrAddInstaPhoto(with: $0)
                }
                //TODO: delete leftover photos
                let photoIDsFromServer = photoJSONs.map{$0["id"].stringValue}
                //instaPhotos
                _ = self.instaPhotos
                    .filter {!photoIDsFromServer.contains(($0 as! InstaPhoto).id!)}
                    .map{self.context.delete($0 as! NSManagedObject)}
                self.context.performSave()
            }
        }) {error in pr(error)}
    }
    
    func updateInfo(){
        APIUser.shared.getUserFromServer(with: id, success: { [weak self] (json) in
           
            guard let self = self else {
                return
            }
            
            self.context.perform {
                self.fill(with: json)
                self.context.performSave()
            }
        }) { (error) in pr("couldnt load updated profile") }
    }
    
    
    
    convenience init?(jsonForRegistration: JSON, entity: NSEntityDescription, insertInto: NSManagedObjectContext) {
        self.init(json: jsonForRegistration["user"], entity: entity, insertInto: insertInto)
        self.isNewlyRegistered  = jsonForRegistration["is_newly_registered"].boolValue
        
        let context = CoreDataStack.shared.managedContext
        settings = Settings(context: context)
        
        if let matches = jsonForRegistration["matches"].array{
            do {
                let fetchedResults = try context.fetch(User.fetchRequest()) as! [User]
                //Если локально нет такого профиля
                let shouldLoadMatches = matches.map{Int64($0["id"].intValue)}.filter {!fetchedResults.map{$0.id}.contains( $0 )}
                shouldLoadMatches.forEach{
                    APIUser.shared.getUserFromServer(with: $0, success: {(userJSON) in
                        let _ = User(json: userJSON, entity: User.entity(), insertInto: context)
                        self.context.performSave()
                    }, failure: { (error) in pr(error)  })
                }
            } catch {
                print ("fetch task failed", error)
            }
        }
    }
    
    func changeOrAddPhoto(with json: JSON){
        let fetchRequest: NSFetchRequest = Photo.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == \(json["id"].intValue)")
        if let result = try? self.context.fetch(fetchRequest), let photo = result.first {
            photo.fill(with: json)
        } else {
            addToPhotos(Photo(json: json, entity: Photo.entity(), insertInto: context))
        }
    }
    
    func changeOrAddInstaPhoto(with json: JSON){
        let fetchRequest: NSFetchRequest = InstaPhoto.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == \(json["id"].intValue)")
        if let result = try? self.context.fetch(fetchRequest), let photo = result.first {
            photo.fill(with: json)
        } else {
            addToInstaPhotos(InstaPhoto(json: json, entity: InstaPhoto.entity(), insertInto: context))
        }
    }
    
    
    override func fill(with json: JSON){
        //pr(json)
        
        if let id = json["id"].int {
            self.id = Int64(id)
        } else if let id = json["profile_id"].int{
            self.id = Int64(id)
        } else {
            pr("""
                Кажется, Коля придумал еще одно имя поля id,
                значит, сегодня мы не взлетим, сорян.
                Коля, определись!
            """); return
        }

        self.gender = Gender(rawValue: Int16(json["gender"].intValue))!
        self.name   = json["name"].string
        self.id     = Int64(id)
        if let birthDate = json["birthday"].string{
            self.birthdate = birthDate.toDate(format: Dates.serverBirthdayFormat)! as NSDate
        }
        
        if let photoJSONs = json["photos"].array{
            photoJSONs.forEach{
                changeOrAddPhoto(with: $0)
            }
        } else if photos.count == 0 && json["first_photo"].dictionary != nil {
            changeOrAddPhoto(with: json["first_photo"])
        }
        
        self.talents = json["profile"]["talents"].arrayValue.map{$0.stringValue}
        self.about = json["profile"]["about"].string
        self.profession = json["profile"]["profession"].string
        self.workPlace = json["profile"]["work"].string
        self.studyPlace = json["profile"]["education"].string
        self.isInstagramConnected = json["is_instagram_attached"].boolValue
    }
    
}
