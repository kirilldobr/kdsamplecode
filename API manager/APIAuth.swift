//
//  AuthHelper.swift
//  Totemo
//
//  Created by Kirill Dobryakov on 12/9/17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class APIAuth: NSObject{
    
    static let shared = APIAuth()
    
    func loginPhone(in controller: UIViewController, success: @escaping SuccessWithUserJSONAndToken, failure: @escaping Failure){
        PhoneLoginHelper.shared.verifyPhone(in: controller, success: { (phone, code) in
            self.authRequest(parameters: ["code": code], success: success, failure: failure)
        }, failure: failure)
    }
    
    func loginSocial(in controller: UIViewController, provider: SocialNetwork, success: @escaping SuccessWithUserJSONAndToken, failure: @escaping Failure){
        switch provider {
        case .facebook:
            FBHelper.shared.auth(in: controller, success: { (userID, userToken) in
                self.authRequest(parameters: ["provider": provider.rawValue, "token": userToken], success: success, failure: failure)
            }) { error in failure(error! as NSError) }
        case .vkontakte:
            VKHelper.shared.auth(in: controller, success: { (userID, userToken) in
                self.authRequest(parameters: ["provider": provider.rawValue, "token": userToken], success: success, failure: failure)
            }) {error in failure(error! as NSError) }
        }
    }
    
    
    private func authRequest(parameters: [String: Any], success: @escaping SuccessWithUserJSONAndToken, failure: @escaping Failure){
        
        let isSocial = parameters.contains { (key, value) -> Bool in return key == "provider" }
        let path = isSocial ? URLs.auth.loginSocial: URLs.auth.loginPhone
        
        Alamofire.request(path, method: .post, parameters: parameters, encoding: URLEncoding.httpBody).responseJSON { (response) in
        
            guard let value = response.value else{ failure(NSError(domain: "Сервер затупил на запрос \(path)", code: -1, userInfo: nil)); return }
            
            let json = JSON(value)

            let resultDic = isSocial ? json["result"].dictionary : json["user"].dictionary
            let successUser = isSocial ? json["result"] : json
            let successToken = isSocial ? json["result"]["token"]["token"].string : json["token"].string
            
            if resultDic != nil {
                pr("👍 \(path.withoutBase())")
                success(successUser, successToken!)
            } else {
                pr("\(path.withoutBase()) error \(ResponseError.with(json: json, code: response.response!.statusCode))")
                failure(ResponseError.with(json: json, code: response.response!.statusCode))
            }

        }
    }
}


