//
//  AddProfilePhotoTableViewController.swift
//  Totemo
//
//  Created by Kirill Dobryakov on 1/14/18.
//  Copyright © 2018 Kirill Dobryakov. All rights reserved.
//

import UIKit
import DKImagePickerController
import SwiftyJSON

class ProfilePhotoCollectionViewController: UICollectionViewController {
    
    lazy var photoCollectionViewLayout: UICollectionViewFlowLayout = {
        var l = UICollectionViewFlowLayout()
        l.minimumInteritemSpacing = photoCellSpacing
        l.minimumLineSpacing = photoCellSpacing
        l.itemSize = CGSize(width: cellWidth, height: cellWidth)
        l.sectionInset = UIEdgeInsets(top: 0, left: edgeInset, bottom: 0, right: edgeInset)
        l.headerReferenceSize = CGSize(width: view.frame.width, height: 107)
        l.footerReferenceSize = CGSize(width: view.frame.width, height: 81)
        l.sectionFootersPinToVisibleBounds = true
        return l
    }()
    
    var photos = [Photo]()
    
    //Либо он вызвался в процессе регистрации, либо из профиля
    var fromProfile = false
    
    let photoCellSpacing: CGFloat = 14
    let edgeInset: CGFloat = 24
    var cellWidth: CGFloat{
        get{
            return (collectionView!.frame.width - 2 * photoCellSpacing - 2 * edgeInset) / 3
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.collectionViewLayout = photoCollectionViewLayout
        navigationController?.setNavigationBarHidden(false, animated: true)
        collectionView!.register(UINib(nibName: "PhotoCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCell")
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        collectionView!.addGestureRecognizer(longPressGesture)
        
        photos = []
        (User.me!.photos.array as! [Photo]).forEach() {
            photos.append($0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        CoreDataStack.shared.managedContext.perform {
            self.photos.rearrange(from: sourceIndexPath.item, to: destinationIndexPath.item)
            self.setAvatarIndicator()
        }
    }
    
    func setAvatarIndicator(){
        for i in 0..<photos.count{
            if let cell = collectionView!.cellForItem(at: IndexPath(item: i, section: 0)) as? PhotoCell{
                cell.setIsAvatar(i == 0)
            }
        }
    }
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = collectionView!.indexPathForItem(at: gesture.location(in: collectionView!)) else { break }
            collectionView!.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            collectionView!.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            guard let selectedIndexPath = collectionView!.indexPathForItem(at: gesture.location(in: collectionView!)) else {
                collectionView!.cancelInteractiveMovement()
                break
            }
            if photos.count == selectedIndexPath.item{
                collectionView!.cancelInteractiveMovement()
            } else{
                collectionView!.endInteractiveMovement()
            }
            
        default:
            collectionView!.cancelInteractiveMovement()
        }
    }
    
    @objc func nextPressed() {
        
        if self.photos.count == 0 && !fromProfile {
            showAlert(title: "Внимание!", message: "Добавьте хотя бы одну фотографию!")
            return
        }
        
        let context = CoreDataStack.shared.managedContext
        let numberOfShouldLoadPhotos = self.photos.filter({ $0.type == .local }).count
        
        if numberOfShouldLoadPhotos == 0{
            self.rearrangePhotosAndPop(fromProfile: fromProfile)
        }
        
        var numberOfLoadedPhotos = 0
        
        for (index, photo) in self.photos.enumerated(){
            if photo.type == .local{
                photo.asset!.fetchFullScreenImage(completeBlock: { (im, info) in
                    APIUserPhoto.shared.uploadPhoto(photo: im!, fractionCompleted: {[weak self]  (fraction) in
                       
                        guard let `self` = self else {
                            return
                        }
                        
                        if let cell = self.collectionView!.cellForItem(at: IndexPath(item: index, section: 0)) as? PhotoCell{
                            cell.setProgressTo(value: fraction)
                        }
                    }, success: { (photoJSON) in
                        numberOfLoadedPhotos += 1

                        context.perform {
                            self.photos[index].data = UIImagePNGRepresentation(im!)! as NSData
                            self.photos[index].fill(with: photoJSON)
                            self.photos[index].asset = nil
                            
                            User.me!.addToPhotos(self.photos[index])
                            if let cell = self.collectionView!.cellForItem(at: IndexPath(item: index, section: 0)) as? PhotoCell{
                                cell.setProgressTo(value: 100)
                            }
                            
                            if numberOfLoadedPhotos == numberOfShouldLoadPhotos{
                                self.rearrangePhotosAndPop(fromProfile: self.fromProfile)
                            }
                        }
                      
                    }, failure: { error in   numberOfLoadedPhotos += 1
                        if numberOfLoadedPhotos == numberOfShouldLoadPhotos{
                            self.rearrangePhotosAndPop(fromProfile: self.fromProfile)
                        }
                    })
                })
            }
        }
    }

    
    func rearrangePhotosAndPop(fromProfile: Bool){
        if fromProfile{
            var photosOrder = [Int]()
            self.photos.forEach {photosOrder.append(Int($0.id))}
            APIUserPhoto.shared.changeOrder(order: photosOrder, success: { [weak self] in
               
                
                guard let `self` = self else {
                    return
                }
                
                let context = CoreDataStack.shared.managedContext
                
                context.perform {
                    (User.me!.photos.array as! [Photo]).forEach{
                        User.me!.removeFromPhotos($0)
                    }
                    self.photos.forEach{
                        User.me!.addToPhotos($0)
                    }
                    context.performSave()
                }
                
                User.me!.getPhotos()
                
                self.navigationController!.popViewController(animated: true)
           
            }, failure: { (error) in pr(error)})
        } else {
            self.pushInitialViewControllerFrom(storyboardName: "SettingsAboutTableViewController") { vc in
                (vc as! SettingsAboutTableViewController).fromProfile = false
            }
                
        }
    }
}

extension ProfilePhotoCollectionViewController: FooterButtonCellDelegate{
    func footerButtonPressed(){
        nextPressed()
    }
}

extension ProfilePhotoCollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return indexPath.row < photos.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count + 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.delegate = self
        
        if indexPath.row == photos.count{
            cell.setAddPhotoCell()
        } else{
            cell.setPhotoCell(photo: photos[indexPath.row], isAvatar: indexPath.item == 0)
        }
        
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader{
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", for: indexPath)
            return headerView
        }
        else{
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "footerView", for: indexPath) as! FooterButtonCollectionView
            footerView.delegate = self
            footerView.button.setTitle(fromProfile ? "Сохранить" : "Продолжить", for: .normal)
            return footerView
        }
    }
}
